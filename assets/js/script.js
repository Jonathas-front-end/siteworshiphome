$(document).ready(function(){
    $(window).scroll(function(){
        // sticky navbar on scroll script
        if(this.scrollY > 20){
            $('.navbar, .menuBtn, .navbar-mobile, a, #imgedf, img').addClass("sticky");
        }else{
            $('.navbar, .menuBtn, .navbar-mobile, a, #imgedf, img').removeClass("sticky");
        }
        
        // scroll-up button show/hide script
        if(this.scrollY > 500){
            $('.scroll-up-btn').addClass("show");
        }else{
            $('.scroll-up-btn').removeClass("show");
        }
    });

    // slide-up script
    $('.scroll-up-btn').click(function(){
        $('html').animate({scrollTop: 0});
        // removing smooth scroll on slide-up button click
        $('html').css("scrollBehavior", "auto");
    });

    $('.navbar .menu li a').click(function(){
        // applying again smooth scroll on menu items click
        $('html').css("scrollBehavior", "smooth");
    });

    // toggle menu/navbar script
    $('.menu-btn').click(function(){
        $('.navbar .menu').toggleClass("active");
        $('.menu-btn img').toggleClass("active");
    });

    // typing text animation script
    var typed = new Typed(".typing", {
        strings: ["Palavras", "Louvor", "Dança", "Teatro"],
        typeSpeed: 100,
        backSpeed: 60,
        loop: true
    });
    var typed = new Typed(".typing-2", {
        strings: ["Adoração", "Comunhão", "Discipulado", "Serviço", "Evangelismo"],
        typeSpeed: 100,
        backSpeed: 60,
        loop: true
    });

    // owl carousel script
    $('.carousel').owlCarousel({
        margin: 20,
        loop: true,
        autoplayTimeOut: 2000,
        autoplayHoverPause: true,
        responsive: {
            0:{
                items: 1,
                nav: false
            },
            600:{
                items: 2,
                nav: false
            },
            1000:{
                items: 3,
                nav: false
            }
        }
    });
});

var btnMute = document.querySelector('.mute');
var btnNoMute = document.querySelector('.nomute');

btnMute.addEventListener('click', function(){
    var vid = document.getElementById("play-video");
    vid.muted = false;

    btnMute.style.display = "none";
    btnNoMute.style.display = "flex";
});

btnNoMute.addEventListener('click', function(e){
    e.preventDefault();

    var vid = document.getElementById("play-video");
    vid.muted = true;

    btnMute.style.display = "flex";
    btnNoMute.style.display = "none";
});

window.addEventListener('scroll', function() {
    const parallax = document.querySelector('.home-content');
    let scrollPosition = window.pageYOffset;
    parallax.style.transform = 'translateY(' + scrollPosition * 0.39 + 'px)';
});